Events.Views.Campaigns ||= {}

class Events.Views.Campaigns.EditView extends Backbone.View
  template: JST["backbone/templates/campaigns/edit"]

  events:
    "click #edit-campaign": "update"

  update: (e) ->
    e.preventDefault()
    e.stopPropagation()
    @form.commit()
    @model.save(null,
      success: (campaign) =>
        @model = campaign
        window.location.hash = "/#{@model.id}"
    )

  render: ->
    @$el.html @template
    @form = new Backbone.Form model: @model
    @$el.append @form.render().el
    return this
