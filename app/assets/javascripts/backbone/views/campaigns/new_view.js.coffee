Events.Views.Campaigns ||= {}

class Events.Views.Campaigns.NewView extends Backbone.View
  template: JST["backbone/templates/campaigns/new"]

  events:
    "click #new-campaign": "save"

  constructor: (options) ->
    super(options)
    @model = new @collection.model()

    @model.bind("change:errors", () =>
      this.render()
    )

  save: (e) ->
    e.preventDefault()
    e.stopPropagation()

    @form.commit()
    @model.unset("errors")

    @collection.create(@model.toJSON(),
      success: (campaign) =>
        @model = campaign
        window.location.hash = "/#{@model.id}"

      error: (campaign, jqXHR) =>
        @model.set({errors: $.parseJSON(jqXHR.responseText)})
    )

  render: ->
    @$el.html @template
    @form = new Backbone.Form model: @model
    @$el.append @form.render().el
    return this
