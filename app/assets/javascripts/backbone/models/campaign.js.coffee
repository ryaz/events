class Events.Models.Campaign extends Backbone.Model
  paramRoot: 'campaign'

  defaults:
    name: null
    start: null
    end: null
    countries: null

  schema:
    name: 'Text'
    start: 'DateTime'
    end: 'DateTime'
    countries:
      type: 'List'
      itemType: 'Object'
      subSchema:
        country:
          type: 'Select'
          validators: ['required']
          options: ['Sweden', 'England', 'USA', 'Russia', 'Canada']
        languages:
          type: 'Select'
          options: ['russian', 'english', 'german', 'spanish', 'chinese']

class Events.Collections.CampaignsCollection extends Backbone.Collection
  model: Events.Models.Campaign
  url: '/campaigns'
