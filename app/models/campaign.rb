class Campaign < ActiveRecord::Base
  include ActiveModel::ForbiddenAttributesProtection
  serialize :countries, JSON
end
