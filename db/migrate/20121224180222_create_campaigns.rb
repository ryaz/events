class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.string :name
      t.datetime :start
      t.datetime :end
      t.text :countries

      t.timestamps
    end
  end
end
