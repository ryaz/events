Feature: Post campaigns

  In order to store campaigns with datetime, countries and languages
  A user
  Should be abble to post campaign
  
  @javascript
  Scenario: Post campaign wo countries and languages

    Given I open "campaigns" click "New Campaign" and write "German trip"
    When I ve clicked "Create"
    Then I should see "German trip"
    
  @javascript
  Scenario: Post campaign with country

    Given I open "campaigns" click "New Campaign" and write "German trip"
    And I ve clicked "Add"
    And I fill in "Country" with "Germany"
    And I ve clicked "OK"
    And I ve clicked "Create"
    Then I should see "German trip"
    And I should see "object"
    
  @javascript
  Scenario: Post campaign with country and language

    Given I open "campaigns" click "New Campaign" and write "German trip"
    And I ve clicked "Add"
    And I fill in "Country" with "Germany"
    And I fill in "languages" with "English"
    And I ve clicked "OK"
    And I ve clicked "Create"
    Then I should see "object"
    And I should see "German trip"