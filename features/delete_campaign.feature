Feature: delete country

  In order to campaign
  A user
  Should be abble to delete campaign
  
  @javascript
  Scenario: Delete campaign

    Given I add campaign "German trip"
    When I ve clicked "Destroy"
    Then I should not see "German trip"