Given %{I open "$path" click "$link" and write "$text"} do |path, link, text|
  step %{I follow "#{path}"}
  step %{I ve clicked "#{link}"}
  step %{I fill in "Name" with "#{text}"}
end

Given %{I follow "$path"} do |path|
  visit root_path
end

Then %{I ve clicked "$link"} do |link|
  click_on link
end

And %{I fill in "$name" with "$text"} do |name, text|
  fill_in name, with: text
end

Then %{I should see "$content"} do |content|
  page.should have_content content
end

Given %{I add campaign "$name"} do |name|
  step %{I open "campaigns" click "New Campaign" and write "#{name}"}
  step %{I ve clicked "Create"}
  step %{I follow "campaigns"}
  step %{I should see "#{name}"}
end 