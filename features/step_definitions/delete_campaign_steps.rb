Then %{I should not see "$content"} do |content|
  within_table('campaigns-table') do
    page.should_not have_content content
  end
end
